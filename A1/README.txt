Created By: Corwin Belser (cmb539)
Class: CS275-002
Title: Assignment One

Code: DropBoxMover.java
Throws: TembooException, IOException, FileNotFoundException
Requirements: Google gson jar file, Temboo jar file
Description: This program will use DropBox's API to look for a file titled
	"_list" which contains names of files to download and local locations
	to download them.  The program will attempt to locate each file and
	download each to its destination.

Testing: The program was tested on my own DropBox account and verified each file
	was downloaded correctly.

Personal Experience: I was wary testing this program too much as it consumes a lot
	of temboo calls.  I also had trouble using temboo to upload a file, as I
	kept getting errors saying I was missing inputs.

Potential Improvements: There isn't much I feel should be changed about this lab.
	I felt the directions were clear and the material was interesting.  It is
	a little impractical as I don't see a user having a file on their DropBox
	with files and locations for a one-time move.