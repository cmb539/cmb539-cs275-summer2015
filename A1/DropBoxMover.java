import java.io.*;
import java.util.Scanner;

import org.apache.commons.codec.binary.Base64;

import com.google.gson.*;
import com.temboo.Library.Dropbox.FilesAndMetadata.*;
import com.temboo.Library.Dropbox.FilesAndMetadata.GetFile.*;
import com.temboo.Library.Dropbox.FilesAndMetadata.SearchFilesAndFolders.*;
import com.temboo.Library.Dropbox.OAuth.*;
import com.temboo.Library.Dropbox.OAuth.FinalizeOAuth.*;
import com.temboo.Library.Dropbox.OAuth.InitializeOAuth.*;
import com.temboo.core.*;

public class DropBoxMover {

	public static void main(String[] args) throws TembooException, IOException, FileNotFoundException {

		Scanner sc = new Scanner(System.in);
		String dropboxAppKey = "o5xgvmeqw8jlvx4";
		String dropboxAppSecret = "4wn0aoziszv27g8";
		
		// Instantiate the Choreo, using a previously instantiated TembooSession object, eg:
		TembooSession session = new TembooSession("cmb539", "DropBoxMover", "1pquMZaliSxQSX89ofuTpRQWWPRcwl5e");
		
		InitializeOAuth initializeOAuthChoreo = new InitializeOAuth(session);

		// Get an InputSet object for the choreo
		InitializeOAuthInputSet initializeOAuthInputs = initializeOAuthChoreo.newInputSet();

		// Set inputs
		initializeOAuthInputs.set_DropboxAppKey(dropboxAppKey);
		initializeOAuthInputs.set_DropboxAppSecret(dropboxAppSecret);
		// Execute Choreo
		InitializeOAuthResultSet initializeOAuthResults = initializeOAuthChoreo.execute(initializeOAuthInputs);
		
		System.out.println("Go to the following URL and allow access:");
		System.out.println(initializeOAuthResults.get_AuthorizationURL());
		System.out.println("Press enter to continue");
		sc.nextLine();
		
		FinalizeOAuth finalizeOAuthChoreo = new FinalizeOAuth(session);

		// Get an InputSet object for the choreo
		FinalizeOAuthInputSet finalizeOAuthInputs = finalizeOAuthChoreo.newInputSet();

		// Set inputs
		finalizeOAuthInputs.set_DropboxAppKey(dropboxAppKey);
		finalizeOAuthInputs.set_DropboxAppSecret(dropboxAppSecret);
		finalizeOAuthInputs.set_CallbackID(initializeOAuthResults.get_CallbackID());
		finalizeOAuthInputs.set_OAuthTokenSecret(initializeOAuthResults.get_OAuthTokenSecret());

		// Execute Choreo
		FinalizeOAuthResultSet finalizeOAuthResults = finalizeOAuthChoreo.execute(finalizeOAuthInputs);
		
		String accessToken = finalizeOAuthResults.get_AccessToken();
		String accessTokenSecret = finalizeOAuthResults.get_AccessTokenSecret();
		
		SearchFilesAndFolders searchFilesAndFoldersChoreo = new SearchFilesAndFolders(session);

		System.out.println("Trying to find a _list file...");
		
		// Get an InputSet object for the choreo
		SearchFilesAndFoldersInputSet searchFilesAndFoldersInputs = searchFilesAndFoldersChoreo.newInputSet();

		// Set inputs
		searchFilesAndFoldersInputs.set_AccessToken(accessToken);
		searchFilesAndFoldersInputs.set_AccessTokenSecret(accessTokenSecret);
		searchFilesAndFoldersInputs.set_AppKey(dropboxAppKey);
		searchFilesAndFoldersInputs.set_AppSecret(dropboxAppSecret);
		searchFilesAndFoldersInputs.set_Query("_list");
		
		// Execute Choreo
		SearchFilesAndFoldersResultSet searchFilesAndFoldersResults = searchFilesAndFoldersChoreo.execute(searchFilesAndFoldersInputs);

		JsonParser jp = new JsonParser();
		JsonElement root = jp.parse(searchFilesAndFoldersResults.get_Response());
		JsonArray rootArray = root.getAsJsonArray();
		
		if(rootArray.size() < 1)
		{
			System.out.println("I couldn't find the _list file!");
		}
		else
		{
			System.out.println("I found the _list file!");
			String filePath = rootArray.get(0).getAsJsonObject().get("path").getAsString();
			
			GetFile getFileChoreo = new GetFile(session);

			// Get an InputSet object for the choreo
			GetFileInputSet getFileInputs = getFileChoreo.newInputSet();

			// Set inputs
			getFileInputs.set_AccessToken(accessToken);
			getFileInputs.set_AccessTokenSecret(accessTokenSecret);
			getFileInputs.set_AppKey(dropboxAppKey);
			getFileInputs.set_AppSecret(dropboxAppSecret);
			getFileInputs.set_Path(filePath);
			
			// Execute Choreo
			GetFileResultSet getFileResults = getFileChoreo.execute(getFileInputs);
			String fileContent = new String(Base64.decodeBase64(getFileResults.get_Response()));
			
			String[] files = fileContent.split("\r\n");
			System.out.println("I found " + files.length + " files to copy!");
			
			//Perform the download for each file
			for(int i = 0; i < files.length; i++)
			{
				String[] file = files[i].split(" ");
				System.out.println("");
				System.out.println("Looking for file: " + file[0] + " on Dropbox...");
				//Locate each file
				SearchFilesAndFolders searchFilesAndFoldersChoreo1 = new SearchFilesAndFolders(session);

				// Get an InputSet object for the choreo
				SearchFilesAndFoldersInputSet searchFilesAndFoldersInputs1 = searchFilesAndFoldersChoreo1.newInputSet();

				// Set inputs
				searchFilesAndFoldersInputs1.set_AccessToken(accessToken);
				searchFilesAndFoldersInputs1.set_AccessTokenSecret(accessTokenSecret);
				searchFilesAndFoldersInputs1.set_AppKey(dropboxAppKey);
				searchFilesAndFoldersInputs1.set_AppSecret(dropboxAppSecret);
				searchFilesAndFoldersInputs1.set_Query(file[0]);
				
				// Execute Choreo
				SearchFilesAndFoldersResultSet searchFilesAndFoldersResults1 = searchFilesAndFoldersChoreo1.execute(searchFilesAndFoldersInputs1);

				JsonParser jp1 = new JsonParser();
				JsonElement root1 = jp1.parse(searchFilesAndFoldersResults1.get_Response());
				JsonArray rootArray1 = root1.getAsJsonArray();
				
				if(rootArray1.size() < 1)
				{
					System.out.println("Couldn't find file: " + file[0]);
					System.out.println("Skipping!");
				}
				else
				{
					String filePath1 = rootArray1.get(0).getAsJsonObject().get("path").getAsString();
					System.out.println("Found file at: " + filePath1);
					
					//Get the file contents
					GetFile localFileChoreo = new GetFile(session);

					// Get an InputSet object for the choreo
					GetFileInputSet fileInputs = localFileChoreo.newInputSet();

					// Set inputs
					fileInputs.set_AccessToken(accessToken);
					fileInputs.set_AccessTokenSecret(accessTokenSecret);
					fileInputs.set_AppKey(dropboxAppKey);
					fileInputs.set_AppSecret(dropboxAppSecret);
					fileInputs.set_Path(filePath1);
					
					// Execute Choreo
					GetFileResultSet getResults = localFileChoreo.execute(fileInputs);
					String content = new String(Base64.decodeBase64(getResults.get_Response()));

					//Print the contents to a the local location
					Writer wr = new BufferedWriter(new OutputStreamWriter(
							new FileOutputStream(file[1] + file[0]), "utf-8"));
					wr.write(content);
					wr.close();
					System.out.println("Finished copying " + file[0] + " to " + file[1] + file[0]);

				}
			}
			System.out.println("Finished copying all files!");
			sc.close();
		}
	}

}
