package cmb539.l3;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class TicTacToe extends ActionBarActivity {

    private String _turn;
    private boolean _hasWon;

    private class SearchResult {
        public final String winner;
        public final boolean hasWinner;

        public SearchResult(String winner, boolean hasWinner){
            this.winner = winner;
            this.hasWinner = hasWinner;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tic_tac_toe);

        _turn = "X";
        _hasWon = false;
        TextView tv = (TextView)findViewById(R.id.textView);
        tv.setText("Player Turn: " + _turn);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tic_tac_toe, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onButtonClick(View v) {
        Button b = (Button)v;
        if(b.getText().length() == 0 && _hasWon == false) {
            b.setText(_turn);
            _turn = (_turn.equals("X")) ? "O" : "X";
            TextView tv = (TextView)findViewById(R.id.textView);

            if(isStalemate())
            {
                tv.setText("It's A Tie!");
            }
            else
            {
                SearchResult eval = hasWinner();
                if(eval.hasWinner)
                {
                    tv.setText("Player " + eval.winner + " Has Won!");
                    _hasWon = true;
                }
                else
                {
                    tv.setText("Player Turn: " + _turn);
                }
            }
        }
    }

    private boolean isStalemate()
    {
        boolean isStalemate = true;
        for(int i = 0; i < 3; i ++)
        {
            for(int j = 0; j < 3; j++)
            {
                int resID = getResources().getIdentifier("Button_" + i + j, "id", getPackageName());
                String text = (String)((Button)findViewById(resID)).getText();
                if(text.equals(""))
                {
                    isStalemate = false;
                }
            }
        }

        return isStalemate;
    }
    private SearchResult hasWinner()
    {
        String[][] grid = new String[3][3];
        for(int i = 0; i < 3; i ++)
        {
            for(int j = 0; j < 3; j++)
            {
                int resID = getResources().getIdentifier("Button_" + i + j, "id", getPackageName());
                grid[i][j] = (String)((Button)findViewById(resID)).getText();
            }
        }
        SearchResult result = new SearchResult("N", false);

        //Check for horizontal win
        for(int i = 0; i < 2; i++)
        {
            if(!grid[i][0].equals("") && grid[i][0].equals(grid[i][1]) && grid[i][0].equals(grid[i][2]))
            {
                result = new SearchResult(grid[i][0], true);
            }
        }

        //Check for vertical win
        for(int i = 0; i < 2; i++)
        {
            if(!grid[0][i].equals("") && grid[0][i].equals(grid[1][i]) && grid[0][i].equals(grid[2][i]))
            {
                result = new SearchResult(grid[0][i], true);
            }
        }

        //Check for diagonal win
        if(!grid[0][0].equals("") && grid[0][0].equals(grid[1][1]) && grid[0][0].equals(grid[2][2]))
        {
            result = new SearchResult(grid[0][0], true);
        }
        if(!grid[2][0].equals("") && grid[2][0].equals(grid[1][1]) && grid[2][0].equals(grid[0][2]))
        {
            result = new SearchResult(grid[2][0], true);
        }

        return result;
    }
}
