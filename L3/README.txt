Created By: Corwin Belser (cmb539)
Class: CS275-002
Title: Lab Three

Description: This android app will facilitate a two-player game of Tic-Tac-Toe.
	Text at the top will inform the players of the current player's turn, as
	well as notifying a winner or a tie.

Testing: I tested the app using Gennymotion as my emulator, running my code through
	Android Studio.  I tested each of the win conditions, as well as the tie
	condition.  I also tested to ensure that pressing a button that is already
	taken will not change.

Personal Experience: I had major trouble with getting the emulator to run on my
	machine.  I tried Eclipse which gave me errors upon creating a project.  I
	then tried Android Studio and was able to create a project.  The emulator
	that came with Android Studio would not run as I have an AMD processor, so
	I had to search a lot for a solution.  I ended up finding Gennymotion, which
	I was able to get running on my machine.  The Tic-Tac-Toe code was easy to
	write.  Getting the buttons through java code was also a challenge, as I was
	trying to use for loops.

Potential Improvements: I think this lab was a little too simple, but I feel that was
	probably intended since it is many students first android app.  I think an easy
	addition to the lab would be to require a button to start a new game.  It would
	be easy to code, and would help reinforce the idea of state-machines when coding
	and app.