Created By: Corwin Belser (cmb539)
Class: CS275-002
Title: Lab Two

Code: OAuth.java
Throws: Exception
Requirements: Google gson jar file, Temboo jar file
Description: This program will use Google's Calendar API to retrieve a user's
	Calendars, printing the name of each calendar.  This program will then
	retrieve the events for the first calendar and print each event's name,
	start date, and start time.
	The program will do these actions twice, once forcing the user to manually
	authorize the program through their browser, and the second using Temboo to
	facilitate the authorization process.

Testing: I tested my program on my own account and verified with my calendar that
	each calendar and the events of one were found correctly.

Personal Experience: The major roadblock I ran into in this lab was writing a POST.
	I had used POST's before while on Co-Op using Ajax, so I had to do a lot of
	trial and error searching Google to find the boiler plate code I needed.
	All of this was then taught in the next lecture (I had started the lab early).
	The rest of the lab was easy to finish.

Potential Improvements: The lab should be written more akin to a tutorial than a
	list of objectives.  Some students relied on Piazza to ask questions rather
	than searching the API or Google for a solution, which a tutorial format
	could resolve.  I felt the tasks were simple enough for the lab.  The lecture
	notes are fairly useless, as they mostly consist of lecture points and pseudo
	code.  It would be helpful to format the slides to contain images of sample code
	and notes explaining the code.  This would make them a valuable resource when
	attempting the labs.