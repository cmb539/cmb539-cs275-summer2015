import java.net.*;
import java.util.Scanner;

import com.google.gson.*;
import com.temboo.core.TembooSession;
import com.temboo.Library.Google.Calendar.*;
import com.temboo.Library.Google.Calendar.GetAllCalendars.*;
import com.temboo.Library.Google.Calendar.GetAllEvents.*;

import java.io.*;

public class OAuth {


	public static void main(String[] args) throws Exception {

		RunManual();
				
		RunTemboo();
		
	}
	
	private static void RunManual() throws Exception
	{
		//Have user get Authorization Code
		System.out.println("Initial Auth Link: ");
		System.out.println("https://accounts.google.com/o/oauth2/auth?"
				+ "scope="
					+ "https://www.googleapis.com/auth/calendar.readonly&"
				+ "redirect_uri="
					+ "urn:ietf:wg:oauth:2.0:oob&"
				+ "response_type="
					+ "code&"
				+ "client_id="
					+ "968486004484-vaf9rrc4am6804u0tirkkpkbckaqsgt0.apps.googleusercontent.com");
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the code from the webpage:");
		String code = sc.nextLine();
		sc.close();
		
		//Use Authorization Code to get Access Token
		String params = "code="
				+ code + "&"
				+ "client_id="
				+ "968486004484-vaf9rrc4am6804u0tirkkpkbckaqsgt0.apps.googleusercontent.com&"
				+ "client_secret="
				+ "zgftcu-cA-e3usQVtQqlDbek&"
				+ "redirect_uri=urn:ietf:wg:oauth:2.0:oob&"
				+ "grant_type=authorization_code";
		URL url = new URL("https://www.googleapis.com/oauth2/v3/token");
		byte[] postDataBytes = params.toString().getBytes("UTF-8");
		
		HttpURLConnection conn = (HttpURLConnection)url.openConnection();
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
		conn.setDoOutput(true);
		conn.getOutputStream().write(postDataBytes);
		BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
		
		JsonParser jp = new JsonParser();
		JsonElement root = jp.parse(in);
		JsonObject rootObj = root.getAsJsonObject();
		String access_token = rootObj.get("access_token").getAsString();
		
		//Retrieve the user's list of calendars
		URL calendar = new URL("https://www.googleapis.com/calendar/v3/users/me/calendarList"
				+ "?access_token=" + access_token);
		URLConnection con = calendar.openConnection();
		BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
		
		jp = new JsonParser();
		root = jp.parse(br);
		rootObj = root.getAsJsonObject();
		JsonArray calendarList = rootObj.get("items").getAsJsonArray();
		
		//Print the summary of each calendar
		for (int i = 0; i < calendarList.size(); i++)
		{
			System.out.println("");
			JsonObject cal = calendarList.get(i).getAsJsonObject();
			String summary = cal.get("summary").getAsString();
			System.out.println("Calendar: " + summary);
		}
		
		//Retrieve the list of events for the first calendar
		String calendarID = calendarList.get(0).getAsJsonObject().get("id").getAsString();
		calendar = new URL("https://www.googleapis.com/calendar/v3/calendars/"
			+ calendarID
			+ "/events"
			+ "?access_token=" + access_token);
		con = calendar.openConnection();
		br = new BufferedReader(new InputStreamReader(con.getInputStream()));
		
		jp = new JsonParser();
		root = jp.parse(br);
		rootObj = root.getAsJsonObject();
		JsonArray eventList = rootObj.get("items").getAsJsonArray();
		
		//Print the details for each event
		for(int i = 0; i < eventList.size(); i++)
		{
			JsonObject event = eventList.get(i).getAsJsonObject();
			String date = "None";
			if(event.get("start").getAsJsonObject().has("date"))
				{
					date = event.get("start").getAsJsonObject().get("date").getAsString();
				}
			String time = "None"; 
			if(event.get("start").getAsJsonObject().has("dateTime"))
				{
					time = event.get("start").getAsJsonObject().get("dateTime").getAsString();
				}
			String title = event.get("summary").getAsString();
			
			System.out.println("");
			System.out.println("Event: " + title);
			System.out.println("Start Date: " + date);
			System.out.println("Start Time: " + time);
		}
	}
	
	private static void RunTemboo() throws Exception
	{
		// Instantiate the Choreo, using a previously instantiated TembooSession object, eg:
		TembooSession session = new TembooSession("cmb539", "myFirstApp", "09eaa9b8b52540f5896e535e03481300");

		GetAllCalendars getAllCalendarsChoreo = new GetAllCalendars(session);

		// Get an InputSet object for the choreo
		GetAllCalendarsInputSet getAllCalendarsInputs = getAllCalendarsChoreo.newInputSet();

		// Set credential to use for execution
		getAllCalendarsInputs.setCredential("myProfile");

		// Set inputs

		// Execute Choreo
		GetAllCalendarsResultSet getAllCalendarsResults = getAllCalendarsChoreo.execute(getAllCalendarsInputs);		
		
		BufferedReader br = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(getAllCalendarsResults.get_Response().getBytes())));
		
		JsonParser jp = new JsonParser();
		JsonElement root = jp.parse(br);
		JsonObject rootObj = root.getAsJsonObject();
		JsonArray calendarList = rootObj.get("items").getAsJsonArray();
		
		//Print the summary of each calendar
		for (int i = 0; i < calendarList.size(); i++)
		{
			System.out.println("");
			JsonObject cal = calendarList.get(i).getAsJsonObject();
			String summary = cal.get("summary").getAsString();
			System.out.println("Calendar: " + summary);
		}
		
		//Retrieve the list of events for the first calendar
		String calendarID = calendarList.get(0).getAsJsonObject().get("id").getAsString();
		
		GetAllEvents getAllEventsChoreo = new GetAllEvents(session);

		// Get an InputSet object for the choreo
		GetAllEventsInputSet getAllEventsInputs = getAllEventsChoreo.newInputSet();

		// Set credential to use for execution
		getAllEventsInputs.setCredential("myProfile");

		// Set inputs
		getAllEventsInputs.set_CalendarID(calendarID);

		// Execute Choreo
		GetAllEventsResultSet getAllEventsResults = getAllEventsChoreo.execute(getAllEventsInputs);
		
		br = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(getAllEventsResults.get_Response().getBytes())));

		jp = new JsonParser();
		root = jp.parse(br);
		rootObj = root.getAsJsonObject();
		JsonArray eventList = rootObj.get("items").getAsJsonArray();
		
		//Print details for each event
		for(int i = 0; i < eventList.size(); i++)
		{
			JsonObject event = eventList.get(i).getAsJsonObject();
			String date = "None";
			if(event.get("start").getAsJsonObject().has("date"))
				{
					date = event.get("start").getAsJsonObject().get("date").getAsString();
				}
			String time = "None"; 
			if(event.get("start").getAsJsonObject().has("dateTime"))
				{
					time = event.get("start").getAsJsonObject().get("dateTime").getAsString();
				}
			String title = event.get("summary").getAsString();
			
			System.out.println("");
			System.out.println("Event: " + title);
			System.out.println("Start Date: " + date);
			System.out.println("Start Time: " + time);
		}
	}
}
