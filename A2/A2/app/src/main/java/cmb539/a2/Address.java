package cmb539.a2;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Corey on 8/20/2015.
 */
public class Address implements android.os.Parcelable {

    public String Street;
    public String City;
    public String State;
    public int Zipcode;

    public Address(String _street, String _city, String _state, int _zipcode)
    {
        Street = _street;
        City = _city;
        State = _state;
        Zipcode = _zipcode;
    }

    private Address(Parcel in)
    {
        Street = in.readString();
        City = in.readString();
        State = in.readString();
        Zipcode = in.readInt();
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(Street);
        stringBuilder.append(" ");
        stringBuilder.append(City);
        stringBuilder.append(", ");
        stringBuilder.append(State);
        stringBuilder.append(" ");
        stringBuilder.append(Zipcode);

        return  stringBuilder.toString();
    }

    public void writeToParcel(Parcel out, int flags)
    {
        out.writeString(Street);
        out.writeString(City);
        out.writeString(State);
        out.writeInt(Zipcode);
    }

    public int describeContents()
    {
        return 0;
    }

    public static final Parcelable.Creator<Address> CREATOR = new Parcelable.Creator<Address>() {
        public Address createFromParcel(Parcel in) {
            return new Address(in);
        }

        public Address[] newArray(int size) {
            return new Address[size];
        }
    };

}
