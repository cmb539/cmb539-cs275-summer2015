package cmb539.a2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

public class AddressActivity extends AppCompatActivity {

    private ArrayList<Address> _addresses;
    private String _USER_LOCATION;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);
        Intent intent = getIntent();

        _USER_LOCATION = intent.getStringExtra("USER_LOCATION");
        _addresses = new ArrayList<>();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void btnAdd(View v)
    {
        StoreAddress();

        ClearFields();
    }

    public void btnFinish(View v)
    {
        StoreAddress();
        ClearFields();

        //Send addresses to Map activity
        Intent intent = new Intent(getBaseContext(), MapsActivity.class);
        intent.putParcelableArrayListExtra("ADDRESSES", _addresses);
        intent.putExtra("USER_LOCATION", _USER_LOCATION);
        startActivity(intent);

    }

    private void StoreAddress()
    {
        String Street = ((TextView)findViewById(R.id.txtCity)).getText().toString();
        String City = ((TextView)findViewById(R.id.txtCity)).getText().toString();
        String State = ((TextView)findViewById(R.id.txtState)).getText().toString();
        int Zipcode = Integer.parseInt(((TextView) findViewById(R.id.txtZipcode)).getText().toString());

        _addresses.add(new Address(Street, City, State, Zipcode));
    }

    private void ClearFields()
    {
        ((TextView)findViewById(R.id.txtCity)).setText("");
        ((TextView)findViewById(R.id.txtCity)).setText("");
        ((TextView)findViewById(R.id.txtState)).setText("");
        ((TextView)findViewById(R.id.txtZipcode)).setText("");
    }
}
