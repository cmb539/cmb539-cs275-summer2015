package cmb539.a2;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Debug;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.JsonReader;
import android.util.Log;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

public class MapsActivity extends FragmentActivity {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private String _USER_LOCATION;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        setUpMapIfNeeded();

        Intent intent = getIntent();

        ArrayList<Address> addresses = intent.getParcelableArrayListExtra("ADDRESSES");
        _USER_LOCATION = intent.getStringExtra("USER_LOCATION");

        new ComputeVisitOrder().execute(addresses);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {

    }

    public class ComputeVisitOrder extends AsyncTask<ArrayList<Address>, Integer, ArrayList<LatLng>>
    {
        private String _APIKEY = "AIzaSyBOAsl8wbLcFavyRyeWduArRfm9agu-isI";
        private String _APIREQUEST = "https://maps.googleapis.com/maps/api/directions/json?";
        private String _PLACEREQUEST = "https://maps.googleapis.com/maps/api/place/details/json?placeid=";

        protected ArrayList<LatLng> doInBackground(ArrayList<Address> ... addresses)
        {
            ArrayList<Address> list = addresses[0];
            ArrayList<LatLng> results = new ArrayList<>();

            //Start building the URL
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(_APIREQUEST);

            //Add the Origin
            stringBuilder.append("origin=");
            stringBuilder.append(_USER_LOCATION);
            //Add the Destination
            stringBuilder.append("&destination=");
            stringBuilder.append(_USER_LOCATION);

            stringBuilder.append("&waypoints=optimize:true");

            for(int i = 0; i < list.size(); i++)
            {
                stringBuilder.append("|");
                stringBuilder.append(list.get(i).toString());
            }

            stringBuilder.append("&key=");
            stringBuilder.append(_APIKEY);


            Log.d("URL", stringBuilder.toString());

            try {
                URL url = new URL(stringBuilder.toString());
                HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                conn.setRequestMethod("GET");

                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                String line = br.readLine();
                while(line != null)
                {
                    Log.println(100, "Json", line);
                    line = br.readLine();
                }

                JsonParser jsonParser = new JsonParser();
                JsonElement jsonElement = jsonParser.parse(br);
                JsonObject jsonObject = jsonElement.getAsJsonObject();
                if(jsonObject.get("status").getAsString().equals("OK")) {
                    JsonArray jsonArray = jsonObject.get("geocoded_waypoints").getAsJsonArray();
                    for(int i = 0; i < jsonArray.size(); i++)
                    {
                        try
                        {
                            StringBuilder urlBuilder = new StringBuilder();
                            urlBuilder.append(_PLACEREQUEST);
                            urlBuilder.append(jsonArray.get(i).getAsJsonObject().get("place_id").getAsString());
                            urlBuilder.append("&key=");
                            urlBuilder.append(_APIKEY);

                            URL place_URL = new URL(urlBuilder.toString());
                            HttpsURLConnection place_conn = (HttpsURLConnection)place_URL.openConnection();
                            place_conn.setRequestMethod("GET");

                            BufferedReader place_br = new BufferedReader(new InputStreamReader(place_conn.getInputStream()));

                            JsonParser place_jp = new JsonParser();
                            JsonElement place_je = place_jp.parse(place_br);
                            JsonObject place_jo = place_je.getAsJsonObject();
                            JsonObject place_location = place_jo.get("result").getAsJsonObject()
                                    .get("geometry").getAsJsonObject()
                                        .get("location").getAsJsonObject();

                            results.add(new LatLng(
                                    place_location.get("lat").getAsDouble(),
                                    place_location.get("lng").getAsDouble()));

                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return results;
        }

        protected void onPostExecute(ArrayList<LatLng> list)
        {
            PlotVisits(list);
        }
    }

    public Void PlotVisits(ArrayList<LatLng> place_ids)
    {
        for(int i = 0; i < place_ids.size(); i++)
        {
            mMap.addMarker(new MarkerOptions().position(place_ids.get(i)).title("Stop: " + i));
        }
        return null;
    }
}
