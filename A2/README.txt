Created By: Corwin Belser
Class: CS271-002
Title: Assignment Two

Description:  This android app can be used to plan house visits for 
	open houses.  The user enters their location and the addresses
	of the houses they wish to visit, and the app will compute the
	ideal route to visit all houses.

Testing:  The majority of the program was tested using Gennymotion as
	an android emulator.  The google maps section was not tested
	as I was unable to get google maps installed on the emulator.

Personal Experience: The main issue I had with this lab was the inablity
	to test, since I wasn't able to get google maps on my emulator.
	This made it difficult to write the code that would plot the route
	as I couldn't test it.

Potential Improvements:  This lab had a lot of variables that took much of
	the focus away from android-specific development.  Simplifying the
	task a bit would allow for students to focus more on the android
	coding.