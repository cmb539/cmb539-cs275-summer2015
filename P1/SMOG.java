import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Scanner;

import com.google.gson.*;
import com.temboo.Library.Twitter.OAuth.*;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth.*;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth.*;
import com.temboo.Library.Twitter.Timelines.*;
import com.temboo.Library.Twitter.Timelines.UserTimeline.*;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;

public class SMOG {

	public static void main(String[] args) throws TembooException, IOException {
		
		if(args.length < 1)
		{
			String username = "billnye";
			System.out.println("No username given, running with: " + username);
			CalculateSMOG(username);
		}
		else
		{
			String username = args[0];
			System.out.println("Running SMOG Test On: " + username);
			CalculateSMOG(username);
		}
			
	}
		private static void CalculateSMOG(String username) throws TembooException, IOException{
		String AppKey = "90BUyZMhdfLvBJLWpqJGGiOPs";
		String AppKeySecret = "o9lqliWKMErbp0AslT8E5IL1L7xoRK40PNV93t2dUKIWfMLkAy";

		// Instantiate the Choreo, using a previously instantiated TembooSession object, eg:
		TembooSession session = new TembooSession("cmb539", "myFirstApp", "09eaa9b8b52540f5896e535e03481300");

		InitializeOAuth initializeOAuthChoreo = new InitializeOAuth(session);

		// Get an InputSet object for the choreo
		InitializeOAuthInputSet initializeOAuthInputs = initializeOAuthChoreo.newInputSet();

		// Set inputs
		initializeOAuthInputs.set_ConsumerKey(AppKey);
		initializeOAuthInputs.set_ConsumerSecret(AppKeySecret);
		initializeOAuthInputs.set_ForwardingURL("");
		
		// Execute Choreo
		InitializeOAuthResultSet initializeOAuthResults = initializeOAuthChoreo.execute(initializeOAuthInputs);

		System.out.println("Please authorize the program at this URL: ");
		System.out.println(initializeOAuthResults.get_AuthorizationURL());
		System.out.println("Press Enter when finished.");
		
		Scanner sc = new Scanner(System.in);
		sc.nextLine();
		sc.close();
		
		//Finalize the OAuth process
		FinalizeOAuth finalizeOAuthChoreo = new FinalizeOAuth(session);

		// Get an InputSet object for the choreo
		FinalizeOAuthInputSet finalizeOAuthInputs = finalizeOAuthChoreo.newInputSet();

		// Set inputs
		finalizeOAuthInputs.set_CallbackID(initializeOAuthResults.get_CallbackID());
		finalizeOAuthInputs.set_ConsumerKey(AppKey);
		finalizeOAuthInputs.set_ConsumerSecret(AppKeySecret);
		finalizeOAuthInputs.set_OAuthTokenSecret(initializeOAuthResults.get_OAuthTokenSecret());
		
		// Execute Choreo
		FinalizeOAuthResultSet finalizeOAuthResults = finalizeOAuthChoreo.execute(finalizeOAuthInputs);
		
		String AccessToken = finalizeOAuthResults.get_AccessToken();
		String AccessTokenSecret = finalizeOAuthResults.get_AccessTokenSecret();
		
		
		//
		UserTimeline userTimelineChoreo = new UserTimeline(session);

		// Get an InputSet object for the choreo
		UserTimelineInputSet userTimelineInputs = userTimelineChoreo.newInputSet();

		// Set inputs
		userTimelineInputs.set_AccessToken(AccessToken);
		userTimelineInputs.set_AccessTokenSecret(AccessTokenSecret);
		userTimelineInputs.set_ConsumerKey(AppKey);
		userTimelineInputs.set_ConsumerSecret(AppKeySecret);
		userTimelineInputs.set_Count(30);
		userTimelineInputs.set_ExcludeReplies(true);
		userTimelineInputs.set_ScreenName(username);
		
		// Execute Choreo
		UserTimelineResultSet userTimelineResults = userTimelineChoreo.execute(userTimelineInputs);
				
		//Retrieve the information
		JsonParser jp = new JsonParser();
		JsonElement root = jp.parse(userTimelineResults.get_Response());
		JsonArray array = root.getAsJsonArray();
		
		ArrayList<String> tweets = new ArrayList<String>();
		
		for(int i = 0; i < array.size(); i++)
		{
			JsonObject tweet = array.get(i).getAsJsonObject();
			tweets.add(tweet.get("text").getAsString());
		}
		
		System.out.println("I found " + tweets.size() + "tweets");
		System.out.println("");
		
		//Get the syllables for each tweet
		int number_of_polysyllables = 0;

		for(int i = 0; i < tweets.size(); i++)
		{
			System.out.println("Here We Have A Tweet:");
			System.out.println(tweets.get(i));
			System.out.println("");
			System.out.println("Let's Analyze This Tweet!");
			
			String[] words = tweets.get(i).split(" ");
			for(int j = 0; j < words.length; j++)
			{
				if(words[j].contains("http"))
				{
					System.out.println("Skipping Link: " + words[j]);
				}
				else
				{
					String word = words[j].replaceAll("[^a-zA-z]", "");
					if(word.isEmpty() || word.contains("@") || word.contains("&") || word.contains("#"))
					{
						System.out.println("Skipping Non-Word: " + words[j]);
					}
					else if(word.length() < 4)
					{
						System.out.println("Skipping Too-Small Word: " + words[j]);
					}
					else
					{
						URL url = new URL("http://api.wordnik.com:80/v4/word.json/"
								+ word
								+ "/hyphenation?useCanonical=true&limit=50&api_key=a2a73e7b926c924fad7001ca3111acd55af2ffabf50eb4ae5");
					
						HttpURLConnection conn = (HttpURLConnection)url.openConnection();
						conn.setRequestMethod("GET");
						BufferedReader rdr = new BufferedReader(new InputStreamReader(conn.getInputStream()));
						JsonParser parser = new JsonParser();
						JsonElement je = parser.parse(rdr);
						if(je.isJsonArray())
						{
							JsonArray ja = je.getAsJsonArray();
							int syllables = ja.size();
							System.out.println(words[j] +  ": " + syllables + " syllables...");
							if(syllables >= 3)
								number_of_polysyllables  += syllables;
						}
						else
						{
							System.out.println("Well That's Weird.  I Wasn't Expecting This:");
							System.out.println(je.toString());
						}				
					}
				}
			}
			System.out.println("");
		}
		System.out.println("I found " + number_of_polysyllables + " words with 3 or more syllables!");
		double grade = 1.0430 * Math.sqrt( number_of_polysyllables * (30 / array.size())) + 3.1291;
		System.out.println("Grade Level Required: " + grade);
	}

}
