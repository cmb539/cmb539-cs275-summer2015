Created By: Corwin Belser (cmb539)
Class: CS275-002
Title: Midterm Practicum

Code: SMOG.java
Throws: TembooException, IOException
Requirements: Google's gson jar file, Temboo's jar file
Description: This program, when run with a Twitter user_id, will analyze up to
	30 of the user's recent tweets, and compute the apporximate SMOG reading 
	level for the user.  This is done using Wordnik's API to get the number of
	polysyllabic words for each tweet.

Testing: The program was tested on five twitter accounts through eclipse.  Special checks
	had to be added to avoid attempting a GET request with a link as a word.  Words less
	than four letters were also excluded as the smallest three-syllable word is four letters.
	Characters other than standard letters also stripped out when analyzing words to reduce
	the amount of calls to Wordnik.

Personal Experience: I thought this was a pretty fun assignment.  I didn't have any trouble getting
	tweets or breaking them into words.  Wordnik did give me some trouble as I couldn't figure
	out where to get an API key.  I ended up using the key used in their example section, which
	has worked for me.  I then had some issues with the program breaking midway through due to
	the url containing weird words like an Http link.

Potential Improvements: The Wordnik API seems a little too out-of-date to be reliable for this
	assignment.  Many common words weren't found by Wordnik, which skew the results.  There could
	also be some added information about what we should be outputing to the user:
		Just print the grade result
		Print the polysyllabic words for each tweet as they're analyzed
		Print the syllable count for each word
	I chose to print the program's action for each word, but I know others who chose to only
	print the polysyllabic words.