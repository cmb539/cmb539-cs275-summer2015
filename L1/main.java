import java.io.*;
import com.google.gson.*;
import java.net.URL;
import java.net.URLConnection;

public class main {
	
	//-----
	//Weather Underground Key: 32227a16422a9084
	//-----
	
	/* WARM-UP
	public static void main(String[] args) throws IOException {

		URL url = new URL("https://www.cs.drexel.edu/~augenbdh/cs275_su15/labs/wxunderground.html");
		URLConnection con = url.openConnection();

		InputStream is = con.getInputStream();
		
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		
		String line = null;
		
		while((line = br.readLine()) != null)
		{
			System.out.println(line);
		}
	}
	*/
	
	public static void main(String[] args) throws IOException
	{
		//Perform the GeoLookup
		URL url_geo = new URL("http://api.wunderground.com/api/32227a16422a9084/geolookup/q/autoip.json");
		URLConnection con = url_geo.openConnection();
		InputStream is = con.getInputStream();
		BufferedReader br = new BufferedReader(new InputStreamReader(is));

		JsonParser jp = new JsonParser();
		JsonElement root = jp.parse(br);
		JsonObject rootObj = root.getAsJsonObject();
		
		int zip = rootObj.get("location").getAsJsonObject()
				.get("zip").getAsInt();
		System.out.println("Closest Zipcode: " + zip);
		
		URL url_weather = new URL("http://api.wunderground.com/api/32227a16422a9084/hourly/q/" + zip + ".json");
		con = url_weather.openConnection();
		is = con.getInputStream();
		br = new BufferedReader(new InputStreamReader(is));
		
		jp = new JsonParser();
		root = jp.parse(br);
		rootObj = root.getAsJsonObject();
		
		JsonArray array = rootObj.get("hourly_forecast").getAsJsonArray();
		
		for(int i = 0; i < array.size(); i++)
		{
			System.out.println("");
			JsonObject obj = array.get(i).getAsJsonObject();
			String date = obj.get("FCTTIME").getAsJsonObject().get("pretty").getAsString();
			
			String condition = obj.get("condition").getAsString();
			String temp = obj.get("temp").getAsJsonObject().get("english").getAsString();
			String humidity = obj.get("humidity").getAsString();
			
			System.out.println(date);
			System.out.println("Condition: " + condition);
			System.out.println("Temperature: " + temp);
			System.out.println("Humidity: " + humidity);
		}
	}
}
