Created By: Corwin Belser (cmb539)

Code: main.java
Requirements: Google gson library
Description: This program will use Wunderground to display
	weather information pertaining to the user's location.
	The program will use the IP adress of the user to determine
	the nearest zip code and then display the hourly forcast for
	that zipcode.
Errors: Throws IOException error if the connection to the site fails.
