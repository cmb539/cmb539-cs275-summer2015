package cmb539.l4;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import com.google.gson.*;

public class MainActivity extends AppCompatActivity {

    private String _ZipCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences settings = getPreferences(MODE_PRIVATE);
        int oldHour = settings.getInt("lastRan", -1);
        int newHour = Calendar.getInstance().get(Calendar.HOUR);

        //Replace true with "oldHour != newHour" to use SQLite Database
        if(true)
        {
            new getByInternet(this).execute();
        }
        else
        {
            new getByDataBase(this).execute();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private class WeatherInfo
    {
        Bitmap bMap;
        String Humidity;
        String Condition;
        String Time;
        String Temperature;
    }

    private class WeatherAdapter extends ArrayAdapter<WeatherInfo>
    {
        public WeatherAdapter(Context context, ArrayList<WeatherInfo> weather){
            super(context, 0, weather);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            WeatherInfo weather = getItem(position);

            if(convertView == null){
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.weather_layout, parent, false);
            }

            ImageView imgIcon = (ImageView) convertView.findViewById(R.id.Img_Icon);
            TextView txtTime = (TextView) convertView.findViewById(R.id.txt_Time);
            TextView txtCond = (TextView) convertView.findViewById(R.id.txt_Descr);
            TextView txtTemp = (TextView) convertView.findViewById(R.id.txt_Temp);
            TextView txtHum = (TextView) convertView.findViewById(R.id.txt_Hum);

            txtTime.setText(weather.Time);
            txtCond.setText(weather.Condition);
            txtHum.setText(weather.Humidity);
            txtTemp.setText(weather.Temperature);
            imgIcon.setImageBitmap(weather.bMap);

            return convertView;
        }
    }
    private class getByInternet extends AsyncTask<Void,Void,ArrayList<WeatherInfo>>
    {
        private Context mContext;

        public getByInternet(Context context)
        {
            mContext = context;
        }

        @Override
        protected ArrayList<WeatherInfo> doInBackground(Void... params) {
            ArrayList<WeatherInfo> weather = new ArrayList<>();
            try
            {
                //Perform the GeoLookup
                URL url_geo = new URL("http://api.wunderground.com/api/32227a16422a9084/geolookup/q/autoip.json");
                URLConnection con = url_geo.openConnection();
                InputStream is = con.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(is));

                JsonParser jp = new JsonParser();
                JsonElement root = jp.parse(br);
                JsonObject rootObj = root.getAsJsonObject();

                int zip = rootObj.get("location").getAsJsonObject()
                        .get("zip").getAsInt();

                URL url_weather = new URL("http://api.wunderground.com/api/32227a16422a9084/hourly/q/" + zip + ".json");
                con = url_weather.openConnection();
                is = con.getInputStream();
                br = new BufferedReader(new InputStreamReader(is));

                jp = new JsonParser();
                root = jp.parse(br);
                rootObj = root.getAsJsonObject();

                JsonArray array = rootObj.get("hourly_forecast").getAsJsonArray();

                WeatherInfo labels = new WeatherInfo();
                labels.Time = "D/M : Hr";
                labels.Temperature = "Temp";
                labels.Humidity = "Humidity";
                labels.Condition = "Condition";

                weather.add(labels);

                for(int i = 0; i < array.size(); i++)
                {
                    JsonObject obj = array.get(i).getAsJsonObject();
                    JsonObject time = obj.get("FCTTIME").getAsJsonObject();
                    String date = time.get("mday").getAsString() + "/" +
                                    time.get("mon_abbrev").getAsString() + " : " +
                                    time.get("hour").getAsString();

                    String condition = obj.get("condition").getAsString();
                    String temp = obj.get("temp").getAsJsonObject().get("english").getAsString();
                    String humidity = obj.get("humidity").getAsString();
                    String url_icon = obj.get("icon_url").getAsString();

                    WeatherInfo forcast = new WeatherInfo();
                    forcast.Condition = condition + "!";
                    forcast.Humidity = humidity + "%";
                    forcast.Temperature = temp + "F";
                    forcast.Time = date;
                    try
                    {
                        URL url = new URL(url_icon);
                        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                        connection.setDoInput(true);
                        connection.connect();
                        InputStream input = connection.getInputStream();
                        forcast.bMap = BitmapFactory.decodeStream(input);
                    }
                    catch(Exception e)
                    {
                        e.printStackTrace();
                    }
                    weather.add(forcast);
                }
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }
            return weather;
        }

        @Override
        protected void onPostExecute(ArrayList<WeatherInfo> weather)
        {
            //Store these in the SQL database
            SQLHelper helper = new SQLHelper((mContext));
            SQLiteDatabase db = helper.getWritableDatabase();

            for(int i = 0; i < weather.size(); i++)
            {
                WeatherInfo item = weather.get(i);

                ContentValues values = new ContentValues();
                if(item.bMap != null)
                    values.put("Image", item.bMap.toString());

                values.put("Humidity", item.Humidity);
                values.put("Condition", item.Condition);
                values.put("Time", item.Time);
                values.put("Temperature", item.Temperature);

                db.insert("weather.db", null, values);
            }

            WeatherAdapter itemsAdapter =
                    new WeatherAdapter(mContext, weather);
            ListView listView = (ListView)findViewById(R.id.lv_items);
            listView.setAdapter(itemsAdapter);

            SharedPreferences settings = getPreferences(MODE_PRIVATE);
            SharedPreferences.Editor editor = settings.edit();
            editor.putInt("lastRan", Calendar.getInstance().get(Calendar.HOUR));
            editor.commit();

        }

    }

    private class getByDataBase extends AsyncTask<Void, Void, ArrayList<WeatherInfo>>
    {
        private Context mContext;
        private String[] allColumns = {"Image", "Humidity", "Condition", "Time", "Temperature"};

        public getByDataBase(Context context)
        {
            mContext = context;
        }

        @Override
        protected ArrayList<WeatherInfo> doInBackground(Void... params)
        {
            ArrayList<WeatherInfo> weather = new ArrayList<>();

            SQLHelper helper = new SQLHelper((mContext));
            SQLiteDatabase db = helper.getReadableDatabase();

            Cursor cursor = db.query("weather.db", allColumns, null, null, null, null, null);
            cursor.moveToFirst();
            while(!cursor.isAfterLast())
            {
                WeatherInfo item = new WeatherInfo();
                byte[] imageAsBytes = cursor.getString(1).getBytes();
                item.bMap = BitmapFactory.decodeByteArray(imageAsBytes, 0,imageAsBytes.length );
                item.Humidity = cursor.getString(2);
                item.Condition = cursor.getString(3);
                item.Time = cursor.getString(4);
                item.Temperature = cursor.getString(5);

                weather.add(item);
            }

            return weather;
        }

        @Override
        protected void onPostExecute(ArrayList<WeatherInfo> weather)
        {



            WeatherAdapter itemsAdapter =
                    new WeatherAdapter(mContext, weather);
            ListView listView = (ListView)findViewById(R.id.lv_items);
            listView.setAdapter(itemsAdapter);

        }
    }
}

