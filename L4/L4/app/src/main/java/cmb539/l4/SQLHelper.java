package cmb539.l4;

import android.content.Context;
import android.database.*;
import android.database.sqlite.*;

public class SQLHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "weather.db";
    private static final int DATABASE_VERSION = 1;
    private static final String WEATHER_TABLE_NAME = "weather";
    private static final String WEATHER_TABLE_CREATE =
            "CREATE TABLE " + WEATHER_TABLE_NAME + " (" +
                    "_id integer primary key autoincrement, " +
                    "Image TEXT, " +
                    "Humidity TEXT, " +
                    "Condition TEXT, " +
                    "Time TEXT, " +
                    "Temperature TEXT);";


    public SQLHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL(WEATHER_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int j)
    {

    }

}
