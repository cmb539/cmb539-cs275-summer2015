Created By: Corwin Belser (cmb539)
Class: CS275-002
Title: Lab Four

Description: This android app will retrieve the hourly forecast by the user's zip-code,
	and display the hourly information in a scrolling list.

Testing: I tested the app using Gennymotion as my emulator, running my code through
	Android Studio.

Personal Experience: I was unable to get the SQLite database working, so I had to omit the
	functionality from my submitted program.  The link provided in the Lab was hard to
	follow and I had trouble finding other examples.  I had little trouble with the rest
	of the lab (I gave the Android GPS a quick try, but couldn't get access to it).

Potential Improvements: I would like to see more examples for the SQLite section of the lab.
	The other parts were easy to implement.